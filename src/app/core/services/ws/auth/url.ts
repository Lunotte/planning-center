
import { HTTP_METHODE } from '../../model';
import { AUTH, LOGIN } from '../base.url';

export const authByUrl = {
    login: {
        method: HTTP_METHODE.POST,
        parts: [AUTH, LOGIN]
    }
};
