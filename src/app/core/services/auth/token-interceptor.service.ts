import { Injectable, Injector } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler } from '@angular/common/http';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor {

  constructor(private injector: Injector) { }

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    console.log('Url de la requête', req.url);
    if (req.url === 'http://localhost:9966/api/auth/login') {
      return next.handle(req);
    }

    const authService = this.injector.get(AuthService);

    const tokenizedReq = req.clone({
      setHeaders: {
        'X-Authorization': 'Bearer ' + authService.getToken(),
      }
    });
    return next.handle(tokenizedReq);
  }
}
