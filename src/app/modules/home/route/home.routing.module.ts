import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import { HomeComponent } from '../container/home/home.component';
import { LINE, PLANNING, PROFIL } from 'src/app/configs/routing/routing-module';

const routes: Route[] = [
    {
        path: '',
        component: HomeComponent,
        children: [
            {
              path: LINE,
              loadChildren: 'src/app/modules/line/line.module#LineModule'
            },
            {
              path: PLANNING,
              loadChildren: 'src/app/modules/planning/planning.module#PlanningModule'
            },
            {
              path: PROFIL,
              loadChildren: 'src/app/modules/profil/profil.module#ProfilModule'
            },
            { path: '**', redirectTo: LINE }
        ]
    }
];

@NgModule({
    imports: [
      RouterModule.forChild(routes),
    ],
    exports: [RouterModule]
  })
  export class HomeRoutingModule { }
