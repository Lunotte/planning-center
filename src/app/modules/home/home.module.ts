import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './container/home/home.component';
import { HomeRoutingModule } from './route/home.routing.module';
import { CoreModule } from 'src/app/core/core.module';
import { HomeService } from './services/home.service';
import { TokenInterceptorService } from 'src/app/core/services/auth/token-interceptor.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    CoreModule,
    HomeRoutingModule
  ],
  providers: [HomeService]
})
export class HomeModule { }
