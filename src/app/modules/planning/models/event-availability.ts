import { TypeAvailability } from 'src/app/core/models/type-availability';

export interface EventAvailability {
  id: number;
  type?: TypeAvailability;
  start: Date;
  end: Date;
}
