import { ProfilComponent } from '../container/profil/profil.component';
import { Route, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { ProfilPersonalInformationComponent } from '../components/profil-personal-information/profil-personal-information.component';
import { PROFIL_USERS, PROFIL_GROUP, PROFIL_PERSONAL_INFORMATION } from 'src/app/configs/routing/routing-module';
import { ProfilGroupComponent } from '../components/profil-group/profil-group.component';
import { ProfilUsersComponent } from '../components/profil-users/profil-users.component';

const routes: Route[] = [
  {
      path: '',
      component: ProfilComponent,
      children: [
        {
          path: PROFIL_PERSONAL_INFORMATION,
          component: ProfilPersonalInformationComponent
        },
        {
          path: PROFIL_GROUP,
          component: ProfilGroupComponent
        },
        {
          path: PROFIL_USERS,
          component: ProfilUsersComponent
        },
        { path: '**', redirectTo: PROFIL_PERSONAL_INFORMATION }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule]
})
export class ProfilRoutingModule { }
