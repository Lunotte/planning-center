import { User } from './user';
import { Group } from './group';
import { Comment } from './comment';

export interface Post {
  readonly id: number;
  readonly created: Date;
  readonly updated: Date;
  readonly user: User;
  readonly group: Group;
  readonly comments: Comment[];
  readonly message: string;
}
