import { WsDefinition } from '../../model';
import { availibilityUrl as URL } from './urls';

export const getAvailibilities = (): WsDefinition => ({
  url: URL.getAvailibilities
});
