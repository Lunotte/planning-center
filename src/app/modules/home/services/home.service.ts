import { Injectable } from '@angular/core';
import { HttpGeneralService } from 'src/app/core/services/http-general.service';
import { getAvailibilities } from 'src/app/core/services/ws/availability/web.service';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  constructor(private httpService: HttpGeneralService) { }

  public testGetAll() {
    this.httpService.requestFromWs<any>(getAvailibilities())
            .toPromise()
            .then((data) => {
                console.log(data);
            })
            .catch(err => {
               console.log(err);
            });
  }
}
