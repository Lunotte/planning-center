import { Component, OnInit, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';
import { RequestType } from 'src/app/core/models/request-type';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { LineCommentForm } from '../../models/line-comment';
import { PostService } from '../../services/post.service';

@Component({
  selector: 'app-add-post',
  templateUrl: './add-post.component.html',
  styleUrls: ['./add-post.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AddPostComponent implements OnInit {

  @Output() submitAddPost = new EventEmitter<{post: LineCommentForm}>();

  ckRequest = false;
  ckSurvey = false;
  requestsType: string[] = [];
  propetierRequestType = '';
  formAddPost: FormGroup;
  fr: any;

  constructor(private postService: PostService) { }

  ngOnInit() {
    this.formAddPost = new FormGroup({
      request: new FormControl(false),
      survey: new FormControl(false),
      start: new FormControl(null),
      end: new FormControl(null),
      requestsType: new FormArray([]),
      message: new FormControl('', [Validators.required])
    }, [validPost]);

    this.fr = {
      firstDayOfWeek: 0,
      dayNames: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
      dayNamesShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
      dayNamesMin: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
      monthNames: [ 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August',
      'September', 'October', 'November', 'December' ],
      monthNamesShort: [ 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' ],
      today: 'Today',
      clear: 'Clear',
      dateFormat: 'mm/dd/yy',
      weekHeader: 'Wk',
      locale: 'fr'
  };
  }

  public switchSurveyRequest(e): void {
    this.ckSurvey = e.checked;
  }

  public switchRequest(e): void  {
    this.ckRequest = e.checked;
  }

  public removeRequestType(index: number): void {
    this.requestsType.splice(index, 1);
    const requestsType = this.formAddPost.get('requestsType') as FormArray;
    requestsType.removeAt(index);
  }

  public addRequestType() {
    this.requestsType.push(this.propetierRequestType);
    const requestsType = this.formAddPost.get('requestsType') as FormArray;
    requestsType.push(new FormControl(this.propetierRequestType));

    this.propetierRequestType = '';
  }

  public addPost() {
    const post: LineCommentForm = this.formAddPost.getRawValue();
    this.submitAddPost.next({post});
    this.formAddPost.reset();
    this.requestsType = [];
  }

}

function validPost(control: FormControl) {
  const value: LineCommentForm = control.value;

  if (value.request && !value.survey) {
    if (value.start === null || value.end === null || value.requestsType.length === 0) {
      return {formNotComplete: true};
    }

    if (value.start > value.end) {
      return {formDateIncorrect: 'Dates are invalide'};
    }
  }

  if (value.request && value.survey) {
    if (value.requestsType.length === 0) {
      return {formNotComplete: true};
    }
  }
  return null;
}
