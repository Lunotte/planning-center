import { Injectable } from '@angular/core';
import { HttpGeneralService } from 'src/app/core/services/http-general.service';
import { saveCommentToPost, savePost, updatePost, deleteRequest } from 'src/app/core/services/ws/post/web.service';
import { Observable } from 'rxjs';
import { Post } from 'src/app/core/models/post';
import { LineComment, LineCommentForm } from '../models/line-comment';
import { Publication } from 'src/app/core/models/publication';
import { ModalEventPlanning } from '../../planning/models/modal-event-planning';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(private httpService: HttpGeneralService) { }

  public savePost(newPost: LineCommentForm): Observable<Publication[]> {
    return this.httpService.requestFromWs<Publication[]>(savePost(newPost));
  }

  public updatePost(id: number, newPost: ModalEventPlanning): Observable<Publication> {
    return this.httpService.requestFromWs<Publication>(updatePost(id, newPost));
  }

  public saveComment(id: number, newComment: LineComment): Observable<Publication[]> {
    return this.httpService.requestFromWs<Publication[]>(saveCommentToPost(id, newComment));
  }

  public deleteRequest(eventId: number): Observable<Publication> {
    return this.httpService.requestFromWs<Publication>(deleteRequest(eventId));
  }

}
