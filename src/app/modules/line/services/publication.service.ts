import { Injectable } from '@angular/core';
import { HttpGeneralService } from 'src/app/core/services/http-general.service';
import { Publication } from 'src/app/core/models/publication';
import { Observable } from 'rxjs';
import { getPublications, saveCommentToRequest, addRequestType } from 'src/app/core/services/ws/publication/web.service';

@Injectable({
  providedIn: 'root'
})
export class PublicationService {

  constructor(private httpService: HttpGeneralService) { }

  public getAll(): Observable<Publication[]> {
    return this.httpService.requestFromWs<Publication[]>(getPublications());
  }

  public saveCheckedRequestType(id: number, idRequestType: number): Observable<Publication[]> {
    return this.httpService.requestFromWs<Publication[]>(saveCommentToRequest(id, idRequestType));
  }

  public addRequestType(id: number, requestType: string): Observable<Publication[]> {
    return this.httpService.requestFromWs<Publication[]>(addRequestType(id, requestType));
  }

}
