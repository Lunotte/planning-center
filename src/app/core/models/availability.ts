import { TypePlanning } from './type-planning';
import { User } from './user';
import { TypeAvailability } from './type-availability';

export interface Availability {
  readonly id: number;
  readonly created: Date;
  readonly updated: Date;
  readonly start: Date;
  readonly end: Date;
  readonly user: User;
  readonly type: TypeAvailability;
}
