import { User } from './user';

export interface Comment {
  readonly id: number;
  readonly created: Date;
  readonly updated: Date;
  readonly masterComment: Comment;
  readonly message: string;
  readonly user: User;
}
