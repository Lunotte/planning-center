import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlanningComponent } from './container/planning/planning.component';
import { PlanningRoutingModule } from './route/planning.routing.module';
import { FullCalendarModule } from '@fullcalendar/angular';
import { ModalAvailabilityComponent } from './components/modal-availability/modal-availability.component';
import {DialogModule} from 'primeng/dialog';
import {SelectButtonModule} from 'primeng/selectbutton';
import { ButtonModule } from 'primeng/button';
import {CalendarModule} from 'primeng/calendar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModalEventComponent } from './components/modal-event/modal-event.component';

@NgModule({
  declarations: [PlanningComponent, ModalAvailabilityComponent, ModalEventComponent],
  imports: [
    CommonModule,
    PlanningRoutingModule,
    FullCalendarModule,
    FormsModule,
    ReactiveFormsModule,
    DialogModule,
    SelectButtonModule,
    ButtonModule,
    CalendarModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PlanningModule { }
