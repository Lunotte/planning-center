import { Injectable } from '@angular/core';
import { HttpGeneralService } from 'src/app/core/services/http-general.service';
import { getPlannings, postAvailability, putAvailability, deleteAvailability } from 'src/app/core/services/ws/planning/web.service';
import { Planning } from 'src/app/core/models/planning';
import { Observable } from 'rxjs';
import { EventAvailability } from '../models/event-availability';

@Injectable({
  providedIn: 'root'
})
export class PlanningService {

  constructor(private httpService: HttpGeneralService) { }

  public getAll(): Observable<Planning[]> {
    return this.httpService.requestFromWs<Planning[]>(getPlannings());
  }

  public postAvailability(eventAvailability: EventAvailability): Observable<EventAvailability> {
    return this.httpService.requestFromWs<EventAvailability>(postAvailability(eventAvailability));
  }

  public putAvailability(eventAvailability: EventAvailability): Observable<EventAvailability> {
    return this.httpService.requestFromWs<EventAvailability>(putAvailability(eventAvailability.id, eventAvailability));
  }

  public deleteAvailability(eventId: number): Observable<EventAvailability> {
    return this.httpService.requestFromWs<EventAvailability>(deleteAvailability(eventId));
  }
}
