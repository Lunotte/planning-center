import { HttpClient, HttpHeaders, HttpParameterCodec, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';

import { catchError } from 'rxjs/operators';

import { URLDef, URLPart, WsParams, WsDefinition } from './model';
import { conf } from 'src/app/configs/global-conf';

@Injectable({ providedIn: 'root', deps: [HttpClient] })
export class HttpGeneralService {

    private readonly serviceUrl: string;

    constructor(private http: HttpClient) {

        const url = conf.urlApi;

        if (url.endsWith('/')) {
            this.serviceUrl = url.substring(0, url.length - 2);
        } else {
            this.serviceUrl = url;
        }
    }

    private stringifyPart(part: URLPart, pathVars: any): string {

        let res: string;

        if (typeof part === 'string') {
            res = part;
        } else if (typeof part === 'function') {

            const param = part(pathVars);

            if (param == null) {
                res = null;
            } else if (typeof param === 'string') {
                res = param;
            } else if (typeof param === 'number') {
                res = '' + param;
            } else {
                res = null;
            }
        } else {
            res = null;
        }

        if (res != null) {
            res = res.replace('/', '');
        }

        return res;
    }

    private buildUrl(url: URLDef, pathVars: any): string {
        return url.parts.reduce((acc: string, v: URLPart) => {
            const currVal = this.stringifyPart(v, pathVars);

            if (currVal == null) {
                return acc;
            } else {
                return acc + '/' + currVal;
            }
        },                      this.serviceUrl).toString();
    }

    private buildHttpParams(queryParams: any) {
        let params: HttpParams;
        if (queryParams == null) {
            return null;
        } else {

            return Object.keys(queryParams)
                .reduce((acc: HttpParams, k) => {
                    const val = queryParams[k];
                    let str: string;

                    if (val == null) {
                        return acc;
                    } else if (typeof val === 'string') {
                        str = val;
                    } else if (val instanceof Array) {
                        params = new HttpParams({encoder: new CustomEncoder()});
                        val.forEach(v => {
                            params = params.append(k, v);
                        });
                    } else if (val instanceof Date) {
                        str = val.toISOString();
                    } else if (typeof val === 'boolean' || typeof val === 'number') {
                        str = val.toString();
                    } else {
                        str = JSON.stringify(val);
                    }

                    if ( (params != null)) {
                        const data = params;
                        params = null;
                        return data;
                    } else{
                        return acc.append(k, str);
                    }
                },      new HttpParams({encoder: new CustomEncoder()}));
        }
    }

    public request<Reponse>(url: URLDef, params: WsParams): Observable<Reponse> {

        const urlStr = this.buildUrl(url, params.pathVars);

        const httpParams = this.buildHttpParams(params.queryParams);

        return this.http.request<Reponse>(url.method, urlStr, { params: httpParams, headers: this.buildHeaders(), body: params.body });
    }

    public requestFromWs<Reponse>(wsDef: WsDefinition): Observable<Reponse> {

        const urlStr = this.buildUrl(wsDef.url, wsDef.pathParams);

        const httpParams = this.buildHttpParams(wsDef.queryParams);

        return this.http.request<Reponse>(wsDef.url.method, urlStr, {
                                              params: httpParams,
                                              headers: this.buildHeaders(),
                                              body: wsDef.body,
                                            }).pipe(catchError(this.handleError));
    }

    private handleError(error: HttpErrorResponse) {
      if (error.error instanceof ErrorEvent) {
        // A client-side or network error occurred. Handle it accordingly.
        console.error('An error occurred:', error.error.message);
      } else {
        // The backend returned an unsuccessful response code.
        // The response body may contain clues as to what went wrong,
        console.error(
          `Backend returned code ${error.status}, ` +
          `body was: ${error.error}`);
      }
      // return an observable with a user-facing error message
      return throwError(
        'Something bad happened; please try again later.');
    };

    private buildHeaders() {
       /* const headers: HttpHeaders = new HttpHeaders();

        headers.append('X-Requested-With', 'XMLHttpRequest');
        headers.append('Access-Control-Allow-Origin', '*');
        headers.append('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');

        console.log(headers);*/

        const headers = new HttpHeaders({ 'X-Requested-With': 'XMLHttpRequest',
                                'Content-Type': 'application/json',
                                'Cache-Control': 'no-cache',
                                'Access-Control-Allow-Origin': '*',
                                'Access-Control-Allow-Methods': 'GET, POST, OPTIONS'
                            });
        return headers;
    }
}

class CustomEncoder implements HttpParameterCodec {
    encodeKey(key: string): string {
      return encodeURIComponent(key);
    }

    encodeValue(value: string): string {
        return encodeURIComponent(value);
    }

    decodeKey(key: string): string {
      return decodeURIComponent(key);
    }

    decodeValue(value: string): string {
      return decodeURIComponent(value);
    }
  }
