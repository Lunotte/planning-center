import { RequestType } from './request-type';
import { User } from './user';

export interface CommentSurvey {
  readonly id: number;
  readonly created: Date;
  readonly updated: Date;
  readonly requestType: RequestType;
  readonly nbCommented: number;
  readonly user: User;
}
