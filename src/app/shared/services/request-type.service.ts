import { Injectable } from '@angular/core';
import { HttpGeneralService } from 'src/app/core/services/http-general.service';
import { RequestType } from 'src/app/core/models/request-type';
import { of, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RequestTypeService {

  constructor(private httpService: HttpGeneralService) { }

 /* public getAll(): Observable<RequestType[]> {
    return this.httpService.requestFromWs<RequestType[]>(getRequestTypes());
  }*/
}
