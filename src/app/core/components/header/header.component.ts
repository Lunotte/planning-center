import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { GroupService } from 'src/app/shared/services/group.service';
import { Observable } from 'rxjs';
import { Group } from '../../models/group';
import { LineComment } from 'src/app/modules/line/models/line-comment';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderComponent implements OnInit {

  iptAddGroup = '';
  group$: Observable<Group>;

  constructor(private groupService: GroupService) { }

  ngOnInit() {
  }

  public addGroup() {
    if (this.iptAddGroup.length > 2) {
      let group: Group;
      group = {id: null, name: this.iptAddGroup, picture: null};
      this.group$ = this.groupService.addGroup(group);
      this.iptAddGroup = '';
    }
  }

}
