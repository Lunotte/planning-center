export interface RequestType {
  readonly id: number;
  readonly name: string;
}
