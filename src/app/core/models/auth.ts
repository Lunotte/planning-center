export interface AuthUser {
  readonly username: string;
  readonly password: string;
}

export interface AuthResponse {
  readonly refreshToken: string;
  readonly token: string;
}
