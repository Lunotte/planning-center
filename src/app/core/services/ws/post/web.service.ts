import { WsDefinition } from '../../model';
import { postUrl as URL } from './urls';
import { LineComment, LineCommentForm } from 'src/app/modules/line/models/line-comment';
import { ModalEventPlanning } from 'src/app/modules/planning/models/modal-event-planning';

export const savePost = (post: LineCommentForm): WsDefinition => ({
  url: URL.savePost,
  body: post
});

export const updatePost = (id: number, post: ModalEventPlanning): WsDefinition => ({
  url: URL.updatePost,
  pathParams: {id},
  body: post
});

export const deleteRequest = (eventId: number): WsDefinition => ({
  url: URL.deleteRequest,
  pathParams: {eventId}
});

export const saveCommentToPost = (id: number, lineComment: LineComment): WsDefinition => ({
  url: URL.saveCommentToPost,
  pathParams: {id},
  body: lineComment
});
