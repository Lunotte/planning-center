import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Publication } from 'src/app/core/models/publication';
import { Observable, of } from 'rxjs';
import { PublicationService } from '../../services/publication.service';
import { tap, switchMap, shareReplay } from 'rxjs/operators';
import { TypeLine } from 'src/app/core/models/type-line';
import { AuthService } from 'src/app/core/services/auth/auth.service';
import { Post } from 'src/app/core/models/post';
import { PostService } from '../../services/post.service';
import { LineComment, LineCommentForm } from '../../models/line-comment';
import { GenericService } from 'src/app/shared/services/generic/generic.service';

@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FeedComponent implements OnInit {

  publications$: Observable<Publication[]>;
  selectedRequestsType: string[] = [];

  constructor(private postService: PostService, private publicationService: PublicationService, private authService: AuthService,
              private genericService: GenericService, private cd: ChangeDetectorRef) { }

  ngOnInit() {
    this.loadPublications();
    this.genericService.getBehavior().subscribe( val => {
      if (val === true) {
        this.loadPublications();
        this.cd.markForCheck();
      }
    });
  }

  public loadPublications() {
    this.selectedRequestsType = [];
    this.publications$ = this.publicationService.getAll().pipe(tap(
      p => this.getSelectedRequestsTypeFromUser(p)));
  }

  public onSubmitComment(submit: {post: Post, newComment: LineComment}) {
    this.publications$ = this.postService.saveComment(submit.post.id, submit.newComment);
  }

  public onCheckRequestType(check: {id: number, idRequestType: number}) {
    this.selectedRequestsType = [];
    this.publications$ = this.publicationService.saveCheckedRequestType(check.id, check.idRequestType).pipe(tap(
      p => this.getSelectedRequestsTypeFromUser(p)));
  }

  public onAddPost(submit: {post: LineCommentForm}) {
    this.publications$ = this.postService.savePost(submit.post).pipe(tap(
      p => this.getSelectedRequestsTypeFromUser(p)));
  }

  public onAddRequestType(submit: {requestId: number, requestType: string}) {
    this.publications$ = this.publicationService.addRequestType(submit.requestId, submit.requestType).pipe(tap(
      p => this.getSelectedRequestsTypeFromUser(p)));
  }

  private getSelectedRequestsTypeFromUser(publications: Publication[]) {
    return publications.filter(r => r.typeLine === TypeLine.request)
      .map(r => {
        r.request.surveys.map(t => {
          if (t.user.id === this.authService.getUserId()) {
            this.selectedRequestsType.push(r.request.id + '-' + t.id + '-' + t.requestType.id);
          }
        });
      });
  }
}
