export interface Group {
  readonly id: number;
  readonly name: string;
  readonly picture: string;
}
