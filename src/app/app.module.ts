import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgModule, APP_INITIALIZER, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { SharedModule } from './shared/shared.module';
import { CoreModule } from './core/core.module';
import { AuthService } from './core/services/auth/auth.service';
import { FullCalendarModule } from '@fullcalendar/angular';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule,
    ScrollingModule,
    AppRoutingModule,
    CoreModule,
    SharedModule
  ],
  providers: [AuthService,
    {
      provide: APP_INITIALIZER,
      useFactory: (authService: AuthService) => function() {return authService.authentication()},
      deps: [AuthService],
      multi: true
    }],
  bootstrap: [AppComponent]
})
export class AppModule { }
