import { WsDefinition } from '../../model';
import { groupUrl as URL } from './url';
import { Group } from 'src/app/shared/services/models/group';

export const getGroups = (): WsDefinition => ({
  url: URL.getGroups
});

export const addGroup = (group: Group): WsDefinition => ({
  url: URL.addGroup,
  body: group
});

export const getDefaultGroup = (): WsDefinition => ({
  url: URL.getDefaultGroup,
});

export const updateGroupActif = (groupId: number): WsDefinition => ({
  url: URL.updateGroupActif,
  pathParams: {groupId}
});
