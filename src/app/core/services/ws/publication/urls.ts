
import { HTTP_METHODE, param } from '../../model';
import { PUBLICATIONS, REQUEST } from '../base.url';

export const publicationUrl = {
    getPublications: {
        method: HTTP_METHODE.GET,
        parts: [PUBLICATIONS]
    },
    saveCommentRequest: {
      method: HTTP_METHODE.POST,
      parts: [PUBLICATIONS, REQUEST, param('id'), 'checked', param('typeRequest')]
    },
    addRequestType: {
      method: HTTP_METHODE.POST,
      parts: [PUBLICATIONS, REQUEST, param('id')]
  }
};
