import { TypePlanning } from './type-planning';
import { Availability } from './availability';
import { Request } from './request';

export interface Planning {
  readonly id: number;
  readonly typePlanning: TypePlanning;
  readonly created: Date;
  readonly updated: Date;
  readonly availability: Availability;
  readonly request: Request;
}
