import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import { LINE } from 'src/app/configs/routing/routing-module';
import { LineComponent } from '../components/line/line.component';
import { FeedComponent } from '../container/feed/feed.component';

const routes: Route[] = [
    {
        path: '',
        component: FeedComponent
    }
];

@NgModule({
    imports: [
      RouterModule.forChild(routes),
    ],
    exports: [RouterModule]
  })
  export class LineRoutingModule { }
