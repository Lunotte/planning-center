
import { HTTP_METHODE, param } from '../../model';
import { PLANNINGS, AVAILABILITIES, AVAILABILITY } from '../base.url';

export const planningUrl = {
    getPlannings: {
        method: HTTP_METHODE.GET,
        parts: [PLANNINGS]
    },
    postAvailability: {
      method: HTTP_METHODE.POST,
      parts: [PLANNINGS, AVAILABILITY]
    },
    putAvailability: {
      method: HTTP_METHODE.PUT,
      parts: [PLANNINGS, AVAILABILITY, param('id')]
    },
    deleteAvailability: {
      method: HTTP_METHODE.DELETE,
      parts: [PLANNINGS, AVAILABILITY, param('eventId')]
    }
};
