import { Group } from './group';

export interface User {
  readonly id: number;
  readonly created: Date;
  readonly updated: Date;
  readonly email: string;
  readonly firstname: string;
  readonly lastname: string;
  readonly username: string;
  readonly group: Group;
}
