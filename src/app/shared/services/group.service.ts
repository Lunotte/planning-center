import { Injectable } from '@angular/core';
import { HttpGeneralService } from 'src/app/core/services/http-general.service';
import { Group } from 'src/app/core/models/group';
import { Observable } from 'rxjs';
import { getGroups, getDefaultGroup, updateGroupActif, addGroup } from 'src/app/core/services/ws/group/web.service';

@Injectable({
  providedIn: 'root'
})
export class GroupService {

  constructor(private httpService: HttpGeneralService) { }

  public getAll(): Observable<Group[]> {
    return this.httpService.requestFromWs<Group[]>(getGroups());
  }

  public addGroup(group: Group): Observable<Group> {
    return this.httpService.requestFromWs<Group>(addGroup(group));
  }

  public getDefaultGroup(): Observable<Group> {
    return this.httpService.requestFromWs<Group>(getDefaultGroup());
  }

  public updateGroupActif(groupId: number): Observable<Group> {
    return this.httpService.requestFromWs<Group>(updateGroupActif(groupId));
  }
}
