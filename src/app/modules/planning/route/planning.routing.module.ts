import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import { PlanningComponent } from '../container/planning/planning.component';

const routes: Route[] = [
    {
        path: '',
        component: PlanningComponent
    }
];

@NgModule({
    imports: [
      RouterModule.forChild(routes),
    ],
    exports: [RouterModule]
  })
  export class PlanningRoutingModule { }
