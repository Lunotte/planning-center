import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HOME } from './configs/routing/routing-module';

const routes: Routes = [
  {
    path: '',
    loadChildren: 'src/app/modules/home/home.module#HomeModule'
  }/*,
  { path: '**', redirectTo: HOME }*/
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
