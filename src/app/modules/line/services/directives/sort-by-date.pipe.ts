import { Pipe, PipeTransform } from '@angular/core';
import { Comment } from 'src/app/core/models/comment';

@Pipe({
  name: 'sortByDate'
})
export class SortByDatePipe implements PipeTransform {

  transform(values: Comment[], args?: any): any {
    let newVal = values.sort((a: Comment, b: Comment) => {

      if (a.created > b.created) {
          return 1;
      } else if (a.created < b.created) {
          return -1;
      } else {
          return 0;
      }
  });

    return newVal;
  }

}
