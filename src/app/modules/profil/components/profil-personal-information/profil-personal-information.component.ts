import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profil-personal-information',
  templateUrl: './profil-personal-information.component.html',
  styleUrls: ['./profil-personal-information.component.css']
})
export class ProfilPersonalInformationComponent implements OnInit {

  yearRange: string;

  constructor() { }

  ngOnInit() {
    this.calculYearRange();
  }

  calculYearRange() {
    const endDate = new Date().getFullYear();
    const startDate = endDate - 100;

    this.yearRange = startDate + ':' + endDate;
  }

  save() {
    console.log('save data');

  }
}
