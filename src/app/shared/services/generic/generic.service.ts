import { Injectable } from '@angular/core';
import { Group } from 'src/app/core/models/group';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GenericService {

  protected groupSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor() { }

  setBehavior(behave: boolean) {
    this.groupSubject.next(behave);
  }

  getBehavior(): Observable<boolean> {
      return this.groupSubject.asObservable();
  }
}
