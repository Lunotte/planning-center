export const HOME = 'consult';
export const LINE = 'fil';
export const PLANNING = 'planning';
export const PROFIL = 'profil';
export const PROFIL_PERSONAL_INFORMATION = 'personal-information';
export const PROFIL_GROUP = 'group';
export const PROFIL_USERS = 'users';
