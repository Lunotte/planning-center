import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfilPersonalInformationComponent } from './profil-personal-information.component';

describe('ProfilPersonalInformationComponent', () => {
  let component: ProfilPersonalInformationComponent;
  let fixture: ComponentFixture<ProfilPersonalInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfilPersonalInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfilPersonalInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
