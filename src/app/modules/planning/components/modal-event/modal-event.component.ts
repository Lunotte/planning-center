import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SelectItem } from 'primeng/components/common/selectitem';
import { FormGroup, FormControl, FormArray, FormBuilder } from '@angular/forms';
import { ModalEventPlanning } from '../../models/modal-event-planning';
import { LineCommentForm } from 'src/app/modules/line/models/line-comment';
import { PlanningService } from '../../services/planning.service';
import { PostService } from 'src/app/modules/line/services/post.service';
import { Publication } from 'src/app/core/models/publication';

@Component({
  selector: 'app-modal-event',
  templateUrl: './modal-event.component.html',
  styleUrls: ['./modal-event.component.css']
})
export class ModalEventComponent implements OnInit {

  propetierRequestType = '';
  requestsType: string[] = [];

  events: SelectItem[];

  eventToUpdate: ModalEventPlanning = {id: null, start: new Date(), end: new Date(), message: null, requestsType: []};

  form: FormGroup;

  @Input() display: boolean;
  @Output() closeModalEvent = new EventEmitter<boolean>();
  @Output() eventUpdated = new EventEmitter<Publication>();
  @Output() eventDelete = new EventEmitter<number>();

  @Input() set inptEvent(event: ModalEventPlanning) {
    if (event !== undefined) {
      this.requestsType = [];
      this.eventToUpdate = event;
      this.form.reset();
      this.form.setControl('requestsType', this.fb.array([]));

      this.form.setValue({start: event.start, end: event.end, message: event.message, requestsType: []});
      this.buildFormArray();
    }
  }

  constructor(private postService: PostService, private fb: FormBuilder) {
    this.form = new FormGroup({
      start: new FormControl(new Date()),
      end: new FormControl(null),
      message: new FormControl(null),
      requestsType: this.fb.array([])
    }, [validEvent]);
   }

  ngOnInit() {
    this.form.reset();
  }

  private buildFormArray() {
    this.eventToUpdate.requestsType.map(t => {
      this.addItem(t.name);
    });
  }

  addItem(item) {
    this.requestsType.push(item);
    this.getRequestsType.push(this.fb.control(item));
 }

  get getRequestsType(): FormArray {
    return this.form.get('requestsType') as FormArray;
  }

  public removeRequestType(index: number): void {
    this.requestsType.splice(index, 1);
    const requestsType = this.form.get('requestsType') as FormArray;
    requestsType.removeAt(index);
  }

  public addRequestType() {
    this.requestsType.push(this.propetierRequestType);
    const requestsType = this.form.get('requestsType') as FormArray;
    requestsType.push(new FormControl(this.propetierRequestType));
    this.propetierRequestType = '';
  }

  public onSubmit() {
    const data: ModalEventPlanning = this.form.getRawValue();
    if (this.eventToUpdate.id != null) {
      data.id = this.eventToUpdate.id;
      this.postService.updatePost(data.id, data).subscribe(event => {
        this.eventUpdated.next(event);
        this.closeModal();
      });
    }
  }

  public onDelete() {
    this.postService.deleteRequest(this.eventToUpdate.id).subscribe((val) => {
      if (val) {
        this.eventDelete.next(this.eventToUpdate.id);
        this.closeModal();
      }
    });
  }

  public closeModal() {
    this.closeModalEvent.next(true);
  }
}

function validEvent(control: FormControl) {
  const value: ModalEventPlanning = control.value;
  if (value.start == null || value.message == null || value.message.length === 0 || value.requestsType.length === 0) {
    return {formNotComplete: true};
  }

  if (value.start && value.end) {
    if (value.start > value.end || value.start === value.end) {
      return {formDateIncorrect: 'Dates are invalide'};
    }
  }
  return null;
}
