
export interface ModalEventPlanning {
  id: number;
  start: Date;
  end: Date;
  message: string;
  requestsType: any[];
}
