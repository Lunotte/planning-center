import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfilComponent } from './container/profil/profil.component';
import { ProfilRoutingModule } from './route/profil.routing.module';
import { MenuComponent } from './components/menu/menu.component';
import { ProfilPersonalInformationComponent } from './components/profil-personal-information/profil-personal-information.component';
import { ProfilGroupComponent } from './components/profil-group/profil-group.component';
import { ProfilUsersComponent } from './components/profil-users/profil-users.component';
import { FileUploadModule } from 'primeng/fileupload';
import { PasswordModule } from 'primeng/password';
import { CalendarModule } from 'primeng/calendar';

@NgModule({
  declarations: [ProfilComponent, MenuComponent, ProfilPersonalInformationComponent, ProfilGroupComponent, ProfilUsersComponent],
  imports: [
    CommonModule,
    ProfilRoutingModule,
    FileUploadModule,
    PasswordModule,
    CalendarModule
  ]
})
export class ProfilModule { }
