import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SelectItem } from 'primeng/components/common/selectitem';
import { EventPlanning } from '../../models/event-planning';
import { EventAvailability } from '../../models/event-availability';
import { FormGroup, FormControl } from '@angular/forms';
import { PlanningService } from '../../services/planning.service';

@Component({
  selector: 'app-modal-availability',
  templateUrl: './modal-availability.component.html',
  styleUrls: ['./modal-availability.component.css']
})
export class ModalAvailabilityComponent implements OnInit {

  availabilities: SelectItem[];
  eventToUpdate: EventAvailability = {id: null, start: new Date(), end: new Date()};

  form: FormGroup;

  @Input() display: boolean;
  @Output() closeModalAvailability = new EventEmitter<boolean>();
  @Output() eventAvailabilityAddedOrUpdated = new EventEmitter<EventAvailability>();
  @Output() eventAvailabilityDelete = new EventEmitter<number>();

  @Input() set inptEvent(event: EventAvailability) {
    if (event !== undefined) {
      this.eventToUpdate = event;
      this.form.setValue({start: event.start, end: event.end, type: event.type});
    }
  }

  constructor(private planningService: PlanningService) { }

  ngOnInit() {
    this.availabilities = [
      {label: 'Availability', value: 'AVAILABLE', icon: 'fa fa-fw fa-unlock', styleClass: 'available'},
      {label: 'Unavailability', value: 'UNAVAILABLE', icon: 'fa fa-fw fa-lock', styleClass: 'unavailable'}
    ];

    this.form = new FormGroup({
      start: new FormControl(new Date()),
      end: new FormControl(null),
      type: new FormControl(null)
    }, [validAvailability]);
  }

  public onSubmit() {
    const eventAvailability: EventAvailability = this.form.getRawValue();
    if (this.eventToUpdate.id != null) {
      eventAvailability.id = this.eventToUpdate.id;
      this.planningService.putAvailability(eventAvailability).subscribe(event => {
        this.eventAvailabilityAddedOrUpdated.next(event);
        this.closeModal();
      });
    } else {
      this.planningService.postAvailability(eventAvailability).subscribe(event => {
        this.eventAvailabilityAddedOrUpdated.next(event);
        this.closeModal();
      });
    }

  }

  public onDelete() {
    this.planningService.deleteAvailability(this.eventToUpdate.id).subscribe((val) => {
      if (val) {
        this.eventAvailabilityDelete.next(this.eventToUpdate.id);
        this.closeModal();
      }
    });
  }

  public closeModal() {
    this.closeModalAvailability.next(true);
  }

}

function validAvailability(control: FormControl) {
  const value: EventAvailability = control.value;

  if (value.start == null || value.type == null) {
    return {formNotComplete: true};
  }

  if (value.start && value.end) {
    if (value.start > value.end || value.start === value.end) {
      return {formDateIncorrect: 'Dates are invalide'};
    }
  }
  return null;
}
