import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { PostService } from '../../services/post.service';
import { Post } from 'src/app/core/models/post';
import { LineComment } from '../../models/line-comment';
import { PublicationService } from '../../services/publication.service';
import { Publication } from 'src/app/core/models/publication';
import { CommentSurvey } from 'src/app/core/models/comment-survey';
import { RequestType } from 'src/app/core/models/request-type';
import { Request } from 'src/app/core/models/request';
import { AuthService } from 'src/app/core/services/auth/auth.service';

@Component({
  selector: 'app-line',
  templateUrl: './line.component.html',
  styleUrls: ['./line.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LineComponent {
  @Input() publications: Publication[] = [];
  @Input() selectedRequestsType: string[] = [];

  @Output() submitComment = new EventEmitter<{post: Post, newComment: LineComment}>();
  @Output() submitCheckRequestType = new EventEmitter<{id: number, idRequestType: number}>();
  @Output() submitRequestType = new EventEmitter<{requestId: number, requestType: string}>();

  textValue = '';
  request = '';

  constructor(private authService: AuthService) { }

  public submit(post: Post, comment: string) {
    let newComment: LineComment;
    newComment = {message: comment};
    this.submitComment.next({post, newComment});
  }

  public checkRequestType(id: number, idRequestType: number) {
    this.submitCheckRequestType.next({id, idRequestType});
  }

  public nbRequest(request: Request, type: RequestType) {
    const surveys = request.surveys;
    const survey = surveys.find(s => s.requestType.id === type.id);
    return (survey !== undefined) ? survey.nbCommented : 0;
  }

  public addRequest(requestId: number, requestType: string) {
    if (requestType.length > 0) {
      this.submitRequestType.next({requestId, requestType});
    }
  }

  public concatRequestRequestType(request: number, surveys: CommentSurvey[], type: RequestType): string {
    let d = '';
    if (surveys.length > 0) {
      const survey = surveys.find(s => s.requestType.id === type.id && s.user.id === this.authService.getUserId());
      if (survey !== undefined) {
        d = request + '-' + survey.id + '-' + survey.requestType.id;
      }
    }
    return d.toString();
  }

}
