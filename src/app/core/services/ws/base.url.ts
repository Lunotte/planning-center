export const REQUEST_TYPES = 'request-types';
export const AVAILABILITIES = 'availabilities';
export const AVAILABILITY = 'availability';
export const PUBLICATIONS = 'publications';
export const PLANNINGS = 'plannings';
export const POST = 'post';
export const COMMENTS = 'comment';
export const REQUEST = 'request';
export const GROUPS = 'groups';
export const GROUP = 'group';
export const DEFAULT = 'default';
export const ACTIF = 'actif';

export const AUTH = 'auth';
export const LOGIN = 'login';
