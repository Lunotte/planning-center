import { TypePlanning } from 'src/app/core/models/type-planning';
import { TypeAvailability } from 'src/app/core/models/type-availability';
import { RequestType } from 'src/app/core/models/request-type';

export interface EventPlanning {
  id: number;
  title: string;
  start: Date;
  end: Date;
  type: TypePlanning;
  typeAvailability: TypeAvailability;
  typeRequests?: RequestType[];
  description?: string;
  backgroundColor: string;
}
