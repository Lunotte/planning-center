import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfilUsersComponent } from './profil-users.component';

describe('ProfilUsersComponent', () => {
  let component: ProfilUsersComponent;
  let fixture: ComponentFixture<ProfilUsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfilUsersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfilUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
