import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGrigPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction'; // for dateClick
import { PlanningService } from '../../services/planning.service';
import { Planning } from 'src/app/core/models/planning';
import { TypePlanning } from 'src/app/core/models/type-planning';
import { GenericService } from 'src/app/shared/services/generic/generic.service';
import { EventPlanning } from '../../models/event-planning';
import { FullCalendarComponent } from '@fullcalendar/angular';
import { EventAvailability } from '../../models/event-availability';
import { TypeAvailability } from 'src/app/core/models/type-availability';
import { ModalEventPlanning } from '../../models/modal-event-planning';
import { Publication } from 'src/app/core/models/publication';

@Component({
  selector: 'app-planning',
  templateUrl: './planning.component.html',
  styleUrls: ['./planning.component.css']
})
export class PlanningComponent implements OnInit {

  calendarPlugins = [dayGridPlugin, timeGrigPlugin, interactionPlugin];

  constructor(private planningService: PlanningService, private genericService: GenericService) { }

  title = 'full-calendar';
  defaultDate: '2019-07-01';

  events: any[];
  options: any;
  locale: 'fr';

  calendarEvents = [];
  planningEvent: EventPlanning[];

  displayModalAvailability = false;
  displayModalEvent = false;
  selectedEvent: EventAvailability;
  selectedEventEvent: ModalEventPlanning;

  @ViewChild('calendar') calendarComponent: FullCalendarComponent;

  ngOnInit() {
    this.loadPlannings();
    this.genericService.getBehavior().subscribe( val => {
      if (val === true) {
        this.loadPlannings();
      }
    });

    this.options = {
      defaultDate: '2019-07-01',
      header: {
        left: 'prev,next today myCustomButton',
        center: 'title',
        right: 'dayGridMonth,timeGridWeek,timeGridDay'
      }
    };
  }

  public loadPlannings() {
    this.planningService.getAll().subscribe(e => {
      this.planningEvent = e.length > 0 ? this.buildEvent(e) : [] as EventPlanning[];
    });
  }

  public buildEvent(events: Planning[]): EventPlanning[] {
    if (events.length > 0) {
      return events.map(e => {
        let event: EventPlanning;
        event = {
          id: e.id,
          title: e.typePlanning,
          start: (e.typePlanning === TypePlanning.AVAILABILITY) ? e.availability.start : e.request.start,
          end: (e.typePlanning === TypePlanning.AVAILABILITY) ? e.availability.end : e.request.end,
          type: e.typePlanning,
          typeAvailability: (e.typePlanning === TypePlanning.AVAILABILITY) ? e.availability.type : null,
          backgroundColor: (e.typePlanning === TypePlanning.AVAILABILITY) ?
                            (e.availability.type === TypeAvailability.Available) ? '#63b83c' : '#ca1111' : '#106ebf'
        };
        if (e.typePlanning === TypePlanning.EVENT) {
          event = {...event, description: e.request.description,
                            title: event.title + ' by ' + e.request.user.username,
                            typeRequests: e.request.types};
        }

        return event;
      });
    }
  }

  eventClick($event) {
    if (($event.event.extendedProps as EventPlanning).type === TypePlanning.AVAILABILITY) {
      this.selectedEvent = {
                            id: $event.event.id,
                            start: $event.event.start,
                            end: $event.event.end,
                            type: $event.event.extendedProps.typeAvailability
                          };
      this.displayModalAvailability = true;

    } else {
      this.selectedEventEvent = {
                                  id: $event.event.id,
                                  start: $event.event.start,
                                  end: $event.event.end,
                                  message: $event.event.extendedProps.description,
                                  requestsType: $event.event.extendedProps.typeRequests
                                };
      this.displayModalEvent = true;
    }
  }

  handleSelect($event) {
    this.displayModalAvailability = true;
    this.selectedEvent = {
      id: null,
      start: $event.start,
      end: $event.end,
      type: TypeAvailability.Available
    };

  }

  public onCloseModalAvailability($event) {
    if ($event) {
      this.displayModalAvailability = !$event;
    }
  }

  public onCloseModalEvent($event) {
    if ($event) {
      this.displayModalEvent = !$event;
    }
  }

  public onEventAvailabilityAddedOrUpdated($event: EventAvailability) {
    if ($event) {
      if (this.planningEvent.find(e => e.id === $event.id)) {
        this.updateAvailability($event);
      } else {
        this.addAvailability($event);
      }
    }
  }

  public onEventAvailabilityDelete($event: string) {
    if ($event) {
      this.removeAvailability($event);
    }
  }

  private addAvailability(event: EventAvailability) {
    this.planningEvent = [...this.planningEvent, {
      id: event.id,
      title: event.type,
      start: event.start,
      end: event.end,
      type: TypePlanning.AVAILABILITY,
      typeAvailability: event.type,
      backgroundColor: (event.type === TypeAvailability.Available) ? '#63b83c' : '#ca1111'
    }
    ];
  }

  /**
   * Update event venant de la modal
   * @param event
   */
  private updateAvailability(event: EventAvailability) {
    this.planningEvent = this.planningEvent.map(u => u.id === event.id ?
        ({...u, start: event.start, end: event.end, typeAvailability: event.type,
          backgroundColor: (event.type === TypeAvailability.Available) ? '#63b83c' : '#ca1111'}) : u);
  }

  private removeAvailability(eventId: string) {
    this.planningEvent = this.planningEvent.filter(e => e.id !== parseInt(eventId, 10));
  }


  public onEventUpdated($event: Publication) {
    if ($event) {
      if (this.planningEvent.find(e => e.id === $event.id)) {
        this.updatEvent($event);
      }
    }
  }

  public onEventDelete($event: string) {
    if ($event) {
      this.removeEvent($event);
    }
  }

  private updatEvent(event: Publication) {
    this.planningEvent = this.planningEvent.map(u => u.id === event.id ?
        ({...u, start: event.request.start, end: event.request.end, description: event.request.description,
          typeRequests: event.request.types}) : u);
  }

  private removeEvent(eventId: string) {
    this.planningEvent = this.planningEvent.filter(e => e.id !== parseInt(eventId, 10));
  }
}
