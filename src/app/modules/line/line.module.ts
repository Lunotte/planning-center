import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LineComponent } from './components/line/line.component';
import { LineRoutingModule } from './route/line.routing.module';
import { AddPostComponent } from './components/add-post/add-post.component';
import { CoreModule } from 'src/app/core/core.module';
import {CalendarModule} from 'primeng/calendar';
import {CheckboxModule} from 'primeng/checkbox';
import {DropdownModule} from 'primeng/dropdown';
import {ToggleButtonModule} from 'primeng/togglebutton';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {TooltipModule} from 'primeng/tooltip';
import {ButtonModule} from 'primeng/button';
import { PostService } from './services/post.service';
import { SortByDatePipe } from './services/directives/sort-by-date.pipe';
import { PublicationService } from './services/publication.service';
import { SortByIdPipe } from './services/directives/sort-by-id.pipe';
import { FeedComponent } from './container/feed/feed.component';

@NgModule({
  declarations: [LineComponent, AddPostComponent, SortByDatePipe, SortByIdPipe, FeedComponent],
  imports: [
    CommonModule,
    CoreModule,
    ReactiveFormsModule,
    LineRoutingModule,
    CalendarModule,
    CheckboxModule,
    ToggleButtonModule,
    FormsModule,
    DropdownModule,
    TooltipModule,
    ButtonModule
  ],
  providers: [PostService, PublicationService]
})
export class LineModule { }
