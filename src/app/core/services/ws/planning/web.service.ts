import { WsDefinition } from '../../model';
import { planningUrl as URL } from './urls';
import { EventAvailability } from 'src/app/modules/planning/models/event-availability';

export const getPlannings = (): WsDefinition => ({
  url: URL.getPlannings
});

export const postAvailability = (eventAvailability: EventAvailability): WsDefinition => ({
  url: URL.postAvailability,
  body: eventAvailability
});

export const putAvailability = (id: number, eventAvailability: EventAvailability): WsDefinition => ({
  url: URL.putAvailability,
  pathParams: {id},
  body: eventAvailability
});

export const deleteAvailability = (eventId: number): WsDefinition => ({
  url: URL.deleteAvailability,
  pathParams: {eventId}
});
