import { Post } from './post';
import { TypeLine } from './type-line';
import { Request } from './request';

export interface Publication {
  readonly id: number;
  readonly typeLine: TypeLine;
  readonly created: Date;
  readonly updated: Date;
  readonly post: Post;
  readonly request: Request;
}
