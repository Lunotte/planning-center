import { RequestType } from 'src/app/core/models/request-type';

export interface LineComment {
  readonly message: string;
}

export interface LineCommentForm {
  readonly request: boolean;
  readonly survey: boolean;
  readonly start: Date;
  readonly end: Date;
  readonly requestsType: string[];
  readonly message: string;
}
