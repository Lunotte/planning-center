import { Component, OnInit, OnDestroy, Input, ChangeDetectionStrategy } from '@angular/core';
import { GroupService } from '../../services/group.service';
import { Observable, Subscription } from 'rxjs';
import { Group } from 'src/app/core/models/group';
import { DomSanitizer } from '@angular/platform-browser';
import { toSvg } from 'jdenticon';
import { AuthService } from 'src/app/core/services/auth/auth.service';
import { GenericService } from '../../services/generic/generic.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-list-group',
  templateUrl: './list-group.component.html',
  styleUrls: ['./list-group.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListGroupComponent implements OnInit, OnDestroy {

  groups$: Observable<Group[]>;
  selectedGroup: Group;
  subscribeDefaultGroup: Subscription;
  subscribeUpdateDefaultGroup: Subscription;

  @Input()
  set newGroup(group: Group) {
    if (group != null) {
      this.groups$ = this.groupService.getAll();
      this.subscribeDefaultGroup = this.groupService.getDefaultGroup().subscribe(g => {
        this.selectedGroup = g;
        this.genericService.setBehavior(true);
      });
    }
  }

  constructor(private groupService: GroupService, private sanitizer: DomSanitizer, private authService: AuthService,
              private genericService: GenericService) { }

  ngOnInit() {
    this.loadGroup();
  }

  private loadGroup() {
    this.groups$ = this.groupService.getAll();
    this.subscribeDefaultGroup = this.groupService.getDefaultGroup().subscribe(g => this.selectedGroup = g);
  }

  public generateIcon(picture: string) {
    this.sanitizer.bypassSecurityTrustHtml(toSvg(picture, 100));
  }

  public changeGroup() {
    this.subscribeUpdateDefaultGroup = this.groupService.updateGroupActif(this.selectedGroup.id)
                                        .subscribe( val => this.genericService.setBehavior(true));
  }

  ngOnDestroy(): void {
    this.subscribeDefaultGroup.unsubscribe();
    this.subscribeUpdateDefaultGroup.unsubscribe();
  }
}
