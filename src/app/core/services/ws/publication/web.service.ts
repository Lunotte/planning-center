import { WsDefinition } from '../../model';
import { publicationUrl as URL } from './urls';

export const getPublications = (): WsDefinition => ({
  url: URL.getPublications
});

export const saveCommentToRequest = (id: number, typeRequest: number): WsDefinition => ({
  url: URL.saveCommentRequest,
  pathParams: {id, typeRequest}
});

export const addRequestType = (id: number, typeRequest: string): WsDefinition => ({
  url: URL.addRequestType,
  pathParams: {id},
  body: typeRequest
});
