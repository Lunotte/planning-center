import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sortById'
})
export class SortByIdPipe implements PipeTransform {

  transform(values: any, args?: any): any {
    let newVal = values.sort((a: any, b: any) => {
      if (a.id > b.id) {
          return 1;
      } else if (a.id < b.id) {
          return -1;
      } else {
          return 0;
      }
    });
    return newVal;
  }
}
