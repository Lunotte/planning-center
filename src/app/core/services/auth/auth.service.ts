import { Injectable } from '@angular/core';
import { HttpGeneralService } from '../http-general.service';
import { login } from '../ws/auth/web.service';
import { AuthResponse } from '../../models/auth';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private httpService: HttpGeneralService) { }

  public authentication(): void {
    this.httpService.requestFromWs<AuthResponse>(login({username: 'charly', password: 'test1234'})).subscribe((data: AuthResponse) => {
      localStorage.setItem('token', data.token);
    });

  }

  public getToken(): string {
    return localStorage.getItem('token');
  }

  public getUserId() {
    const helper = new JwtHelperService();
    const decodedToken = helper.decodeToken(this.getToken());

    return parseInt(decodedToken.sub, 10);
  }
}
