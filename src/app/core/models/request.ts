import { User } from './user';
import { Group } from './group';
import { RequestType } from './request-type';
import { CommentSurvey } from './comment-survey';

export interface Request {
  readonly id: number;
  readonly created: Date;
  readonly updated: Date;
  readonly start: Date;
  readonly end: Date;
  readonly user: User;
  readonly group: Group;
  readonly description: string;
  readonly survey: boolean;
  readonly surveys: CommentSurvey[];
  readonly types: RequestType[];
}
