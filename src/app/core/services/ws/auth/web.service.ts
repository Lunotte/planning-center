import { WsDefinition } from '../../model';
import { authByUrl as URL } from './url';
import { AuthUser } from 'src/app/core/models/auth';

export const login = (auth: AuthUser): WsDefinition => ({
  url: URL.login,
  body: auth
});
