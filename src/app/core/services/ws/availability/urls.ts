
import { HTTP_METHODE } from '../../model';
import { AVAILABILITIES } from '../base.url';

export const availibilityUrl = {
    getAvailibilities: {
        method: HTTP_METHODE.GET,
        parts: [AVAILABILITIES]
    }
};
