import { HTTP_METHODE, param } from '../../model';
import { GROUPS, DEFAULT, GROUP, ACTIF } from '../base.url';

export const groupUrl = {
  getGroups: {
      method: HTTP_METHODE.GET,
      parts: [GROUPS]
  },
  addGroup: {
      method: HTTP_METHODE.POST,
      parts: [GROUP]
  },
  getDefaultGroup: {
    method: HTTP_METHODE.GET,
    parts: [GROUP, DEFAULT]
  },
  updateGroupActif: {
    method: HTTP_METHODE.PUT,
    parts: [GROUP, param('groupId'), ACTIF]
  }
};
