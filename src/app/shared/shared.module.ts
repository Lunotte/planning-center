import { NgModule } from '@angular/core';
import { RequestTypeService } from './services/request-type.service';
import { ListGroupComponent } from './components/list-group/list-group.component';
import { DropdownModule } from 'primeng/dropdown';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [ListGroupComponent],
  imports: [
    CommonModule,
    DropdownModule,
    FormsModule
  ],
  exports: [ListGroupComponent],
  providers: [RequestTypeService]
})
export class SharedModule { }
