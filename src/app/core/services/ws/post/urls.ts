import { HTTP_METHODE, param} from '../../model';
import { COMMENTS, PUBLICATIONS, POST, REQUEST } from '../base.url';

export const postUrl = {
  savePost: {
    method: HTTP_METHODE.POST,
    parts: [PUBLICATIONS, POST]
  },
  updatePost: {
    method: HTTP_METHODE.PUT,
    parts: [PUBLICATIONS, POST, param('id')]
  },
  deleteRequest: {
    method: HTTP_METHODE.DELETE,
    parts: [PUBLICATIONS, REQUEST, param('eventId')]
  },
  saveCommentToPost: {
    method: HTTP_METHODE.PUT,
    parts: [PUBLICATIONS, POST, param('id'), COMMENTS]
  }
};
