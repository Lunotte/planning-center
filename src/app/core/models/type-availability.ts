export enum TypeAvailability {
  Available = 'AVAILABLE',
  Unavailable = 'UNAVAILABLE'
}
