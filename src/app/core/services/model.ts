
export enum HTTP_METHODE {
  GET = 'GET',
  PUT = 'PUT',
  POST = 'POST',
  DELETE = 'DELETE'
}

export function param(nom: string) {
  return (pathVars: any) => {
      if (pathVars != null) {
          return pathVars[nom];
      } else {
          return null;
      }
  };
}

export type URLPart = string | ((any) => string);

export interface URLDef {
  readonly method: HTTP_METHODE;
  readonly parts: URLPart[];
}

export interface WsParams {
  readonly queryParams?: any;
  readonly pathVars?: any;
  readonly body?: any;
}

export interface WsDefinition {
  url: URLDef;
  queryParams?: any;
  pathParams?: any;
  body?: any;
}
